My dot files.

1. Clone directory 
2. Download rcm (https://robots.thoughtbot.com/rcm-for-rc-files-in-dotfiles-repos)
3. Set RCRC environment variable 
4. Download [Plug](https://github.com/junegunn/vim-plug#unix)
5. rcup -v
6. On MAC brew install mercurial vim cmake 
7. Open Vim and :PlugInstall
8. cd /.vim/plugged/YouCompleteMe && ./install.py --clang-completer