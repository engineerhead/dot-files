"Set theme
"set termguicolors
colorscheme paramount
set background=dark

"Sensible defaults for Vim {{{
" Make Vim more useful
set nocompatible

"Allow changing buffers
set hidden

" Use the OS clipboard by default (on versions compiled with `+clipboard`)
set clipboard=unnamed

" Tab settings
"set expandtab
"set shiftwidth=2
"set softtabstop=2
set autoindent
set smartindent

" Text display settings
set linebreak
set textwidth=80
set nowrap
set whichwrap+=h,l,<,>,[,]

" Enhance command-line completion
set wildmenu

" Allow cursor keys in insert mode
set esckeys

" Allow backspace in insert mode
set backspace=indent,eol,start

" Optimize for fast terminal connections
set ttyfast

" Add the g flag to search/replace by default
set gdefault

" Use UTF-8 without BOM
set encoding=utf-8 nobomb

" Don’t add empty newlines at the end of files
set binary
set noeol

" Disable backups, swapfiles and centralize undo history
set nobackup
set noswapfile
set history=1000
if exists("&undodir")
    set undodir=~/.vim/undo
endif

" Respect modeline in files
set modeline
set modelines=4

" Enable line numbers
set number

" Enable syntax highlighting
syntax on

" Highlight current line
set cursorline


" Show “invisible” characters
"set lcs=tab:▸\ ,trail:·,eol:¬,nbsp:_
"set list

" Highlight searches
"set hlsearch
" Ignore case of searches
set ignorecase
" Highlight dynamically as pattern is typed
set incsearch

" Always show status line
set laststatus=2

" Enable mouse in all modes
set mouse=a

" Disable error bells
set noerrorbells

" Don’t reset cursor to start of line when moving around.
set nostartofline

" Show the cursor position
set ruler

" Don’t show the intro message when starting Vim
set shortmess=atI

" Show the current mode
set showmode

" Show the filename in the window titlebar
set title

" Show the (partial) command as it’s being typed
set showcmd

" Use relative line numbers
if exists("&relativenumber")
    set relativenumber
    au BufReadPost * set relativenumber
endif

" Start scrolling three lines before the horizontal window border
set scrolloff=3

" Automatic commands
if has("autocmd")
    " Enable file type detection
    filetype plugin indent on
    " Treat .json files as .js
   " autocmd BufNewFile,BufRead *.json setfiletype json syntax=javascript
    " Treat .md files as Markdown
    autocmd BufNewFile,BufRead *.md setlocal filetype=markdown
    " Indent settings for different filetypes
    autocmd Filetype ruby setlocal expandtab sw=2 sts=2
    autocmd Filetype yaml setlocal expandtab sw=2 sts=2
    autocmd Filetype eruby setlocal expandtab sw=2 sts=2
    autocmd Filetype php setlocal expandtab sw=4 sts=4
    autocmd Filetype json setlocal expandtab sw=4 sts=4
endif

"Make and save views
set viewoptions-=options
augroup vimrc
    autocmd BufWritePost *
    \   if expand('%') != '' && &buftype !~ 'nofile'
    \|      mkview
    \|  endif
    autocmd BufRead *
    \   if expand('%') != '' && &buftype !~ 'nofile'
    \|      silent loadview
    \|  endif
augroup END

" Change mapleader
let mapleader=","
set pastetoggle=<F2>
"}}}

"Shortcuts for Vim {{{

"=========================
"Normal Mode Mappings
"=========================
"Set backslash to ESC
"noremap \ <ESC>

"Deleting a buffer
nnoremap <leader>bd :bd<CR>

"Use map leader to move between buffer
nnoremap <leader>l :bn<CR>
nnoremap <leader>h :bp<CR>

"Change command mode key from : to ;
nnoremap ; :

"Change current working directory to where file exists
noremap ,cd :cd %:p:h<CR>

",d to duplicate line
noremap <leader>d Yp

" Insert blank line below or above cursor
noremap [<CR> O<Esc>
noremap ]<CR> o<Esc>

"Save file
nnoremap <leader>w :w<CR>
nnoremap <leader>wq :wq<CR>

"Set splits to open right and below
set splitright
set splitbelow

"=========================
"Insert Mode Mappings
"=========================
"Save File(,w)
inoremap <leader>w <ESC>:w<CR>

"(,d) Duplicate the line where cursor is
inoremap <leader>d <ESC>Yp

"(,dx) Delete the line where cursor is
inoremap <leader>dd <C-O>dd

function! StripWhitespace()
    let save_cursor = getpos(".")
    let old_query = getreg('/')
    :%s/\s\+$//e
    call setpos('.', save_cursor)
    call setreg('/', old_query)
endfunction
noremap <leader>ss :call StripWhitespace()<CR>
" }}}

if has("unix")
  let s:uname = system("uname")
  if s:uname == "Darwin\n"
    " Do Mac stuff here
"Plugins  setup {{{
call plug#begin('~/.vim/plugged')
    "Plug 'Valloric/YouCompleteMe'
    "Plug 'SirVer/ultisnips'
    "Plug 'honza/vim-snippets'

    "Plug 'scrooloose/syntastic'

    Plug 'ctrlpvim/ctrlp.vim'
    Plug 'bling/vim-airline'

    Plug 'valloric/matchtagalways'

    Plug 'Raimondi/delimitMate'
    Plug 'tpope/vim-surround'
    Plug 'scrooloose/nerdcommenter'

    Plug 'sheerun/vim-polyglot'
    Plug 'easymotion/vim-easymotion'

    Plug 'tpope/vim-fugitive'
    Plug 'airblade/vim-gitgutter'

    Plug 'mattn/emmet-vim'

    Plug 'shawncplus/phpcomplete.vim', {'for': 'php'}
    Plug 'dsawardekar/wordpress.vim', {'for': 'php'}

call plug#end()
" }}}

"Plugins Key mappings {{{
"=========================
"Airline settings
"=========================
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_idx_mode = 1
nmap <leader>1 <Plug>AirlineSelectTab1
nmap <leader>2 <Plug>AirlineSelectTab2
nmap <leader>3 <Plug>AirlineSelectTab3
nmap <leader>4 <Plug>AirlineSelectTab4
nmap <leader>5 <Plug>AirlineSelectTab5
nmap <leader>6 <Plug>AirlineSelectTab6
nmap <leader>7 <Plug>AirlineSelectTab7
nmap <leader>8 <Plug>AirlineSelectTab8
nmap <leader>9 <Plug>AirlineSelectTab9

"=========================
" diasble ycm keymapping  to play nice with ultisnips
"=========================
"let g:ycm_key_list_select_completion=['<Up>']
"let g:ycm_key_list_previous_completion=['<Down>']
"let g:UltiSnipsExpandTrigger="<c-j>"
"let g:UltiSnipsListSnippets="<c-tab>"

"let g:ycm_global_ycm_extra_conf ='~/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/ycm/.ycm_extra_conf.py'

"let g:surround_45 = "<% \r %>"
"let g:surround_61 = "<%= \r %>"


"let g:ctrlp_open_new_file = 'r'

"function! g:UltiSnips_Complete()
  "call UltiSnips#ExpandSnippet()
  "if g:ulti_expand_res == 0
    "if pumvisible()
      "return "\<C-n>"
    "else
      "call UltiSnips#JumpForwards()
      "if g:ulti_jump_forwards_res == 0
        "return "\<TAB>"
      "endif
    "endif
  "endif
  "return ""
"endfunction

"function! g:UltiSnips_Reverse()
  "call UltiSnips#JumpBackwards()
  "if g:ulti_jump_backwards_res == 0
    "return "\<C-P>"
  "endif

  "return ""
"endfunction


"if !exists("g:UltiSnipsJumpForwardTrigger")
  "let g:UltiSnipsJumpForwardTrigger = "<tab>"
"endif

"if !exists("g:UltiSnipsJumpBackwardTrigger")
  "let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
"endif

"au InsertEnter * exec "inoremap <silent> " . g:UltiSnipsExpandTrigger     . " <C-R>=g:UltiSnips_Complete()<cr>"
"au InsertEnter * exec "inoremap <silent> " .     g:UltiSnipsJumpBackwardTrigger . " <C-R>=g:UltiSnips_Reverse()<cr>"

""Syntastic settings
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*

"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0
" }}}

let g:airline_theme='deep_space'
  endif
endif