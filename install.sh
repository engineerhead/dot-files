cd ~
apt-add-repository -y ppa:martin-frost/thoughtbot-rcm
apt-get -y install rcm git
git clone https://anepicname@bitbucket.org/anepicname/dot-files.git ~/.dotfiles
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
cp ~/.dotfiles/.rcrc ~/.rcrc
rcup -v
