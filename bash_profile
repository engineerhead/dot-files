if [ -f ~/.bashrc ]; then
	source  ~/.bashrc;
fi
if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi

if [[ "$OSTYPE" == "darwin"* ]]; then
	[[ -s $(brew --prefix)/etc/profile.d/autojump.sh ]] && . $(brew --prefix)/etc/profile.d/autojump.sh
fi

if [ -f /usr/local/bin/aws_completer ]; then
	complete -C '/usr/local/bin/aws_completer' aws
fi
